//
//  ViewController.swift
//  ControlFace
//
//  Created by Esteban Preciado on 3/16/19.
//  Copyright © 2019 Esteban Preciado. All rights reserved.
//

import UIKit
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Desaparece la barra de navegacion (Bateria, hora, etc)
    override var prefersStatusBarHidden: Bool{
        return true
    }

    @IBOutlet weak var ImageView: UIImageView!
    
    //Guardar la imagen
    var inputImage : UIImage? // Guardo la imagen en una variable
    var detectedFaces = [(observation:VNFaceObservation, isBlurred : Bool)]() //Diccionario donde se guardaran las caras
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ImageView.isUserInteractionEnabled = true
        
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(takePhoto)) // Boton llama a la funcion para seleccionar una foto
        
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(sharePhoto))
    }
    
    override func viewDidLayoutSubviews() {
        addRectFaces()
    }
    
    @objc func takePhoto(){
        //Selecciono una imagen y muestro
        let picker = UIImagePickerController()
        picker.allowsEditing = true  //Usuario puede editar la foto
        picker.delegate = self
        present(picker, animated: true)
    }
    
    //Boton Cancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //Abre una ventana donde se podra seleccionar la foto
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {return}
        
        self.inputImage = pickedImage
        self.ImageView.image = self.inputImage
        
        dismiss(animated: true){ //Cierra la ventana
            //Detectar la cara de las fotos
            self.detectFaces()
        }
    }
    
    func detectFaces(){
        guard let inputImage = inputImage else {return}
        guard let ciImage = CIImage(image: inputImage) else {return}
        //VNDetectFaceLandmarksRequest Permite reconocer toda la cara
        //unowned self indica que no quede vinculado al metodo sino a la propia clase
        let request = VNDetectFaceLandmarksRequest{[unowned self] (request, error) in
            if let error = error {
                print(error.localizedDescription)
            }else{
                guard let observations = request.results as? [VNFaceObservation] else {return}
                self.detectedFaces = Array(zip(observations, [Bool](repeating: false, count: observations.count))) //zip permite empaquetar en un diccionario
            }
        }
        
        let handler = VNImageRequestHandler(ciImage: ciImage)
        
        do{
            try handler.perform([request])
        }catch{
            print(error.localizedDescription) //error variable implicita
        }
    }
    
    func addRectFaces(){
        self.ImageView.subviews.forEach {$0.removeFromSuperview()} //Quita todos los rectangulos existentes
        
        let width = self.ImageView.frame.width
        let heigh = self.ImageView.frame.height
        
        for(index, face) in self.detectedFaces.enumerated(){
            //Encontrar la posicion de la cara
            let boundingBox = face.observation.boundingBox //Posicion donde se encuentra la cara
            let size = CGSize(width: boundingBox.width * width, height: boundingBox.height * heigh) //Tamano de la cara
            //Posicion de la cara
            var origin = CGPoint(x: boundingBox.minX * width, y: (1-boundingBox.minY) * heigh - size.height)
            //Colocar ImageView
            let view = UIView(frame: CGRect(origin: origin, size: size))
            view.tag = index
            view.layer.borderColor = UIColor.red.cgColor
            view.layer.borderWidth = 2
            
            self.ImageView.addSubview(view)
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(faceTapped))
            recognizer.numberOfTapsRequired = 1
            view.addGestureRecognizer(recognizer)
        }
    }
    
    //Permite difuminar la cara
    func renderBlurredFaces(){
        guard let uiImage = inputImage else {return}
        guard let cgimage = uiImage.cgImage else {return}
        let ciImage = CIImage(cgImage: cgimage)
        guard let filter = CIFilter(name: "CIPixellate") else {return}
        filter.setValue(ciImage, forKey: kCIInputEVKey)
        filter.setValue(60, forKey: kCIInputScaleKey)
        
        guard let outputImage = filter.outputImage else {return}
        let blurredImage = UIImage(ciImage: outputImage)
        let rendered = UIGraphicsImageRenderer(size: uiImage.size)
        let result = rendered.image{ctx in
            uiImage.draw(at: .zero)
            let path = UIBezierPath()
            
            for face in detectedFaces{
                if face.isBlurred{
                    let boundingBox = face.observation.boundingBox
                    let size = CGSize(width: boundingBox.size.width * uiImage.size.width, height: boundingBox.size.height * uiImage.size.height)
                    let origin = CGPoint(x: boundingBox.minX * uiImage.size.width, y: (1 - boundingBox.minY) * uiImage.size.height - size.height)
                    let rect = CGRect(origin: origin, size: size)
                    path.append(UIBezierPath(rect: rect))
                }
            }
            
            if !path.isEmpty{
                path.addClip()
                blurredImage.draw(at: .zero)
            }
        }
        
        self.ImageView.image = result
    }
    
    @objc func faceTapped(_ sender: UITapGestureRecognizer){
        guard let view = sender.view else {return}
        let tag = view.tag
        self.detectedFaces[tag].isBlurred = !self.detectedFaces[tag].isBlurred
        renderBlurredFaces()
    }
    
    @objc func sharePhoto(){
        guard let image = self.ImageView.image else {return}
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(activityVC, animated: true)
    }
}

